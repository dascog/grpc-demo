package com.example.grpcdemo;

import com.example.grpcdemo.proto.ChatMessage;
import com.example.grpcdemo.proto.ChatMessageFromServer;
import com.example.grpcdemo.proto.ChatServiceGrpc;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class GRpcDemoApplicationTests {
    @GrpcClient("inProcess")
    private ChatServiceGrpc.ChatServiceStub chatClient1;

    @GrpcClient("inProcess")
    private ChatServiceGrpc.ChatServiceStub chatClient2;

    @Test
    void contextLoads() {
    }

    @Test
    public void testChat()
    {
        List messages = new ArrayList<ChatMessageFromServer>();

        StreamObserver chatClient1Observer = generateObserver(messages);
        StreamObserver chatClient2Observer = generateObserver(messages);

        chatClient1.chat(chatClient1Observer);
        chatClient2.chat(chatClient2Observer);

        chatClient1Observer.onNext(generateChatMessage("Hello Chat Client2", "ChatClient1"));
        chatClient2Observer.onNext(generateChatMessage("Hello Chat Client1", "ChatClient2"));

        Assert.notEmpty(messages, "Validate Messages are populated");
    }

    private StreamObserver<ChatMessage> generateObserver(List messages)
    {
        return new StreamObserver<>() {
            @Override
            public void onNext(ChatMessage chatMessage) {
                messages.add(chatMessage);
                System.out.println(chatMessage.getMessage());
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onCompleted() {

            }
        };
    }

    private ChatMessage generateChatMessage(String message, String from)
    {
        return ChatMessage.newBuilder()
                .setMessage(message)
                .setFrom(from)
                .build();
    }
}
