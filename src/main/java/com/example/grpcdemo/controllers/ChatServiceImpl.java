package com.example.grpcdemo.controllers;

import com.example.grpcdemo.proto.ChatMessage;
import com.example.grpcdemo.proto.ChatMessageFromServer;
import com.example.grpcdemo.proto.ChatServiceGrpc;
import io.grpc.stub.StreamObserver;

import java.util.LinkedHashSet;
// package and imports up here
public class ChatServiceImpl extends ChatServiceGrpc.ChatServiceImplBase {

    private static LinkedHashSet<StreamObserver<ChatMessageFromServer>> chatClients = new LinkedHashSet<>();

    @Override
    public StreamObserver<ChatMessage> chat(StreamObserver<ChatMessageFromServer> responseObserver) {
        chatClients.add(responseObserver);

        return new StreamObserver<>() {
            @Override
            public void onNext(ChatMessage chatMessage) {
                for (StreamObserver<ChatMessageFromServer> chatClient : chatClients) {
                    chatClient.onNext(ChatMessageFromServer.newBuilder().setMessage(chatMessage).build());
                }

            }
            // trivial overrides of onError and onCompleted here

            @Override
            public void onError(Throwable throwable) {
                responseObserver.onError(throwable);
            }

            @Override
            public void onCompleted() {
                chatClients.remove(responseObserver);
            }
        };
    }
}